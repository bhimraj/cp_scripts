#!/bin/bash

check_exec(){
  if [ "$?" -ne 0 ]
  then
    echo $1
    exit 1
  fi
}

get_docuum_processes() {
  docuum_processes="$(ps -eo 'tty,pid,comm' | grep -P '\?[[:space:]]*[0-9]{2,}[[:space:]]docuum')"

  printf "\n===> Following are the pseudo terminal docuum processes running \n"

  echo "$docuum_processes"
}

run_docuum_daemon_and_process(){
  echo "===> Starting Docuum Daemon..."
  sudo systemctl start docuum
  sleep 2
  sudo systemctl enable docuum --now
  sleep 2
  echo "===> Spawning Docuum Process In The Background..."
  sudo docuum --threshold '2 GB' &
}

echo "===========> Running Docuum Script..."
echo "===> This Script Should Only Be Run By A Cron Job. Running It Manually Will Result In Failure"

# At any point there should be 2 detached processes running (daemon, actual software)

if [[ $(sudo systemctl status docuum | grep -i "Active: active" | wc -l) == 0 ]]
then
  run_docuum_daemon_and_process
fi

# See the problems in Help

get_docuum_processes

#  detached docuum process count

docuum_processes_count="$(ps -eo 'tty,pid,comm' | grep -P '\?[[:space:]]*[0-9]{2,}[[:space:]]docuum' | wc -l)"

if [ "$docuum_processes_count" -ne 2 ]
then
  sudo systemctl stop docuum

  sleep 2

  # converting multiline process ids to an array
  arr_pids=($(ps -eo 'tty,pid,comm' | grep -P '\?[[:space:]]*[0-9]{2,}[[:space:]]docuum' | grep -P '[0-9]{2,}' -o))

  echo "===> Removing all Docuum Processes"

  for pid in "${arr_pids[@]}"
  do 
    printf "The process id is %s\n" "$pid"
    sudo kill "$pid"
    sleep 1
    count+=1
  done

  run_docuum_daemon_and_process
fi

# Kill docuum processes attached to psuedo-terminal. If someone has ran docuum manually in any of the runner.

pts_docuum_process_count="$(ps -eo 'tty,pid,comm' | grep -P 'pts/[0-9]{1,}[[:space:]]*[0-9]{2,}[[:space:]]docuum' | wc -l)"


if [ "$pts_docuum_process_count" -gt 0 ]
then
  pts_arr_pids=($(ps -eo 'tty,pid,comm' | grep -P 'pts/[0-9]{1,}[[:space:]]*[0-9]{2,}[[:space:]]docuum' | grep -P '[0-9]{2,}' -o))
  for pid in "${pts_arr_pids[@]}"
  do 
    printf "The attached process id is %s\n" "$pid"
    sudo kill "$pid"
    sleep 1
    count+=1
  done
fi

echo "===> Script ran successfully"

exit 0