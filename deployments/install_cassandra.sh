#!/bin/bash

check_exec(){
  if [ "$?" -ne 0 ]
  then 
    echo $1
    exit 1
  fi
}


sudo apt-get update

wget --version

if [ "$?" -ne 0 ]
then
  sudo apt-get install wget
  check_exec '===> Tried installing "wget" but cannot be installed. Try installing it manually'
fi

sudo wget -O xclip.deb kr.archive.ubuntu.com/ubuntu/pool/universe/x/xclip/xclip_0.13-1_amd64.deb

sudo dpkg -i xclip.deb

check_exec '===> Cannot install xclip. It seems that the link is broken'

headless_jre=`sudo apt-get install openjdk-11-jre-headless -y`

check_exec '===> Above "headless_jre" command failed. Open the script file & fix it before continuing'

change_dir=`cd /usr/lib/jvm/java-11-openjdk-amd64/`

check_exec '===> Above "change_dir" command failed. Open the script file & fix the path before continuing'

pwd | xclip

java_path=`xclip -o`

echo "JAVA_HOME=${java_path}" >> ~/.bashrc 

# echo $JAVA_HOME won't work in this terminal instance
# echo $JAVA_HOME

lines=`cat ~/.bashrc | grep "JAVA_HOME" | wc -l`

if [ "$lines" -le 0 ]
then
  echo "===> Alias is not added properly"
fi

echo "deb http://www.apache.org/dist/cassandra/debian 40x main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list

get_keys_res=`wget -qO - https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -`

# substring includes OK
if [[ $get_keys_res == *"OK"* ]]
then
  echo "===> Keys for cassandra procured successfully"
else
  echo "===> Keys for cassandra cannot be procured"
  exit 1
fi

echo "===> Updating package indexes"

sudo apt-get update

check_exec "===> Package indexes cannot be updated"

echo "===> Installing cassandra ........"

sudo apt-get install cassandra -y

check_exec "===> Cassandra cannot be installed"

echo -n "===> Do you want to start Cassandra now ? (y|n) "
read input

# converting to lowercase & comparing
if [ "${input,,}" = "y" -o "${input,,}" = "yes" ]
then
  echo "===> Starting Cassandra ........"
  sudo service cassandra start
  check_exec "===> Cassandra could not be started"
fi

echo "===> Script ran successfully"

exit 0
