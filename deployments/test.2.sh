#!/bin/bash

TIMEFORMAT='===> Elaspsed time %R seconds'

time {

# Installing Node.js with Apt using NodeSource PPA

sudo apt update

# sudo apt upgrade -y :- Causes the script to exit after this command

sudo apt install curl -y

cd ~

echo "===> Installing Node 16.x"
curl -sL https://deb.nodesource.com/setup_16.x -o /tmp/nodesource_setup.sh

sudo bash /tmp/nodesource_setup.sh

sudo apt install nodejs

sudo npm install -g yarn@1.22.10

yarn add global pm2

# start pm2 on startup
pm2 startup

#install git
sudo apt install git -y
cd /home/ubuntu

echo "===> Cloning creds"

git clone https://cp_sec_dt_web_server:8ReZxYPt8RH3-B784w5H@gitlab.com/coin_planet/secrets.git

echo "===> Cloning cp-front"
# get source code from gitlab
git clone https://cp_front_dt_web_server:QgipydA9wyVfkoRuW2Am@gitlab.com/coin_planet/cp-front.git

cp secrets/cp_front.env.local cp-front/
# renaming
mv cp_front.env.local .env.local

#get in project dir
cd cp-front

#give permission
sudo chmod -R 755 .
#install node module
yarn install

yarn 

yarn build

echo "===> Starting cp-front"
pm2 start yarn --name "cpfront" --interpreter bash -- start_prod

cd ..

echo "===> Removing creds"

rm -r secrets

}