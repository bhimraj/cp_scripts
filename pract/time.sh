#!/bin/bash

# WAY 1
start_time=$(date +%s%N)

time {
touch test2.sh
}

end_time=$(date +%s%N)

echo "Elapsed time: $(($(($end_time-$start_time))/1000000)) ms"

# WAY 2

# use "%0R" if u don't want decimal points
TIMEFORMAT='It took %R seconds.'
time {
sleep 5
sleep 7
}
